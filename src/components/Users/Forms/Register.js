import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../../../utils/UserContext';

export default function Register() {
	const navigate = useNavigate(); //tobereplaced

	const { user } = useContext(UserContext); //tobereplaced

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	const capitalizeFirstLetter = (str) => {
		return str.charAt(0).toUpperCase() + str.slice(1);
	};

	const handleSubmit = (e) => {
		e.preventDefault();

		navigate('/login');
	};

	function registerUser(e) {
		e.preventDefault();

		const capitalizedFirstName = capitalizeFirstLetter(firstName);
		const capitalizedLastName = capitalizeFirstLetter(lastName);

		fetch('https://cpstn2-ecommerceapi-macaraig.onrender.com/users/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				firstName: capitalizedFirstName,
				lastName: capitalizedLastName,
				email: email,
				mobileNo: mobileNo,
				password: password,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data) {
					setFirstName('');
					setLastName('');
					setEmail('');
					setMobileNo('');
					setPassword('');
					setConfirmPassword('');

					Swal.fire({
						title: 'Registration Successful',
						icon: 'success',
						text: 'Please proceed to login.',
					}).then((result) => {
						if (result.isConfirmed) {
							navigate('/login');
						}
					});
				} else {
					Swal.fire({
						title: 'Registration failed',
						icon: 'error',
						text: 'An error has occured during registration. Please try again.',
					});
				}
			});
	}

	useEffect(() => {
		if (
			firstName !== '' &&
			lastName !== '' &&
			email !== '' &&
			mobileNo !== '' &&
			password !== '' &&
			confirmPassword !== '' &&
			password === confirmPassword &&
			mobileNo.length === 11
		) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNo, password, confirmPassword]);

	return user.id !== null ? (
		<Navigate to='/' />
	) : (
		<Form onSubmit={(e) => registerUser(e)}>
			<h1 className='my-5 text-center'>Register</h1>

			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control
					type='text'
					placeholder='Enter First Name'
					required
					value={firstName}
					onChange={(e) => {
						setFirstName(e.target.value);
					}}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control
					type='text'
					placeholder='Enter Last Name'
					required
					value={lastName}
					onChange={(e) => {
						setLastName(e.target.value);
					}}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control
					type='email'
					placeholder='Enter Email'
					required
					value={email}
					onChange={(e) => {
						setEmail(e.target.value);
					}}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Mobile No:</Form.Label>
				<Form.Control
					type='text'
					placeholder='Enter Mobile 11 digit No.'
					required
					value={mobileNo}
					onChange={(e) => {
						setMobileNo(e.target.value);
					}}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type='password'
					placeholder='Enter password'
					required
					value={password}
					onChange={(e) => {
						setPassword(e.target.value);
					}}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Confirm Password:</Form.Label>
				<Form.Control
					type='password'
					placeholder='Confirm password'
					required
					value={confirmPassword}
					onChange={(e) => {
						setConfirmPassword(e.target.value);
					}}
				/>
			</Form.Group>

			{isActive ? (
				<Button variant='primary mt-3' type='submit' id='submitBtn'>
					Submit
				</Button>
			) : (
				<Button variant='danger mt-3' type='submit' id='submitBtn'>
					Submit
				</Button>
			)}
		</Form>
	);
}
