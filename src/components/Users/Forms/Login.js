import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../../../utils/UserContext';

export default function Login() {
	const navigate = useNavigate(); //tobereplaced
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function userLogin(e) {
		e.preventDefault();

		fetch('https://cpstn2-ecommerceapi-macaraig.onrender.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email: email,
				password: password,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (typeof data.token !== 'undefined') {
					localStorage.setItem('token', data.token);

					setUser({
						id: data.userRegistered._id,
						isAdmin: data.userRegistered.isAdmin,
					});

					setEmail('');
					setPassword('');

					Swal.fire({
						title: 'Login Successful',
						icon: 'success',
						text: 'Welcome to HAHA Innovations!',
					}).then((result) => {
						if (result.isConfirmed) {
							navigate('/home');
						}
					});
				} else {
					Swal.fire({
						title: 'Authentication failed',
						icon: 'error',
						text: 'Check your detials and login again.',
					});
				}
			});
	}

	useEffect(() => {
		if (email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);

	return user.id !== null ? (
		<Navigate to='/' />
	) : (
		<Container>
			<Form onSubmit={(e) => userLogin(e)}>
				<h1 className='my-5 text-center'>Login</h1>

				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control
						type='email'
						placeholder='Enter Email'
						required
						value={email}
						onChange={(e) => {
							setEmail(e.target.value);
						}}
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type='password'
						placeholder='Enter password'
						required
						value={password}
						onChange={(e) => {
							setPassword(e.target.value);
						}}
					/>
				</Form.Group>

				{isActive ? (
					<Button variant='success mt-3' type='submit' id='submitBtn'>
						Login
					</Button>
				) : (
					<Button variant='secondary mt-3' type='submit' id='submitBtn'>
						Login
					</Button>
				)}
			</Form>
		</Container>
	);
}
