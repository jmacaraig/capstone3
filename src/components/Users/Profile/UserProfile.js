import {useState,useEffect, useContext} from 'react';
import {Row, Col, Container} from 'react-bootstrap';
import UserContext from '../../../utils/UserContext';
import { useNavigate,Navigate } from 'react-router-dom';
// import ResetPassword from '../components/ResetPassword';	
// import UpdateProfileForm from '../components/UpdateProfile';

export default function UserProfile({profile}){

    const {user} = useContext(UserContext);

	return (
		<>
		<Container className='mt-3'>
			<Row>
				<Col className="p-5 bg-primary rounded-lg text-white">
					<h1 className="my-3 ">PROFILE</h1>
					<h2 className="mt-3">{`${profile.firstName} ${profile.lastName}`}</h2>
					<hr />
					<h4>Contacts</h4>
					<ul>
						<li>Email: {profile.email}</li>
						<li>Contact Number: </li>
					</ul>
				</Col>
			</Row>
			<Row className='pt-4 mt-4'>
				<Col>
					<h1>Reset Password</h1>
				</Col>
			</Row>
			<Row className='pt-4 mt-4'>
				<Col>
                    <h1>Update Profile</h1>
				</Col>
			</Row>
		</Container>
		</>
		


	)

}