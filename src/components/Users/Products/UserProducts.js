import React, { useState, useEffect } from 'react';
import { Card, Button, Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function UserProduct({ usProds }) {

	const [products, setProducts] = useState([])

	useEffect(() => {
		const productArr = usProds.map(usProd => {
			if(usProd.isActive === true) {
				return (
					<Col sm={4} key={usProd._id}>
                    <Card className="mt-3">
                    <Card.Body>
                        <Card.Title>{usProd.name}</Card.Title>
						<Card.Subtitle>Brand:</Card.Subtitle>
                        <Card.Text>{usProd.brand === ""? `N/A` : `${usProd.brand}`}</Card.Text>
						<Card.Subtitle>Type:</Card.Subtitle>
                        <Card.Text>{usProd.type === ""? `N/A` : `${usProd.type}`}</Card.Text>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{usProd.description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>PhP {usProd.price}</Card.Text>
                        <Link className="btn btn-primary" to={`/products/${usProd._id}`}>Details</Link>
                    </Card.Body>
                	</Card>
					</Col>
					)
			} else {
				return null;
			}
		})
		setProducts(productArr)

	}, [usProds])

	return(
		<>
		<Container>
			<Row>{ products }</Row>
		</Container>
		</>
		)
}