import { Button, Modal, Form, Table } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';

export default function CreateOrder({ orders, total, getCart, resetCost }) {
	const [activeOrder, setActiveOrder] = useState([]);

	const [showEdit, setShowEdit] = useState(false);

	useEffect(() => {
		const activeOrderArr = orders.map((order) => (
			<tr key={order.productId}>
				<td>{order.name}</td>
				<td>{order.description}</td>
				<td>{order.subTotal}</td>
			</tr>
		));

		setActiveOrder(activeOrderArr);
	}, [orders]);

	const openEdit = () => {
		setShowEdit(true);
	};

	const closeEdit = () => {
		setShowEdit(false);
	};

	const checkOut = (e) => {
		e.preventDefault();

		const data = {
			activeOrder: orders,
			total: total,
		};

		fetch(
			`https://cpstn2-ecommerceapi-macaraig.onrender.com/orders/createCartOrder`,
			{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
				body: JSON.stringify(data),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (data.message === 'Order has been created.') {
					Swal.fire({
						title: 'Order Created!',
						icon: 'success',
						text: 'You have successfully ordered the item.',
					})
						.then(getCart())
						.then(resetCost())
						.then(closeEdit());
				} else {
					Swal.fire({
						title: 'Something went wrong.',
						icon: 'error',
						text: data.message,
					})
						.then(getCart())
						.then(resetCost());
				}
			});
	};

	return (
		<>
			<Button
				className='m-5 bg-primary'
				variant='primary'
				size='m'
				onClick={() => openEdit()}
			>
				Proceed to Checkout
			</Button>
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={(e) => checkOut()}>
					<Modal.Header closeButton>
						<Modal.Title className='text-center'>
							Are you sure you want to place an order on below item(s)?
						</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Table>
							<thead>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>Sub Total</th>
								</tr>
							</thead>
							<tbody>{activeOrder}</tbody>
							<tfoot>
								<tr>
									<th colSpan='2'>Total</th>
									<td>{total}</td>
								</tr>
							</tfoot>
							<tr>
								<td>Note</td>
							</tr>
						</Table>
						<p>Note</p>
					</Modal.Body>

					<Modal.Footer>
						<Button
							className='bg-secondary'
							variant='secondary'
							onClick={closeEdit}
						>
							Close
						</Button>
						<Button
							className='bg-success'
							variant='success'
							type='submit'
							onClick={checkOut}
							onSubmit={closeEdit}
						>
							Submit
						</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
	);
}
