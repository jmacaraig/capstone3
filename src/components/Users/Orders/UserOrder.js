import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';

export default function UserOrder({ userOrders }) {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    const ordersArr = [];

    userOrders.forEach((order, orderIndex) => {
      order.products.forEach((product, productIndex) => {
        const isProductIndexZero = productIndex === 0;

        const row = (
          <tr key={`${orderIndex}-${productIndex}`}>
            {isProductIndexZero ? (
              <>
                <td rowSpan={order.products.length}>{order._id}</td>
              </>
            ) : null}
            <td>{product.name}</td>
            <td>{product.description}</td>
            <td>{product.quantityOrdered}</td>
            <td>{product.price}</td>
            {isProductIndexZero ? (
              <td rowSpan={order.products.length}>{order.totalAmount}</td>
            ) : null}
            {isProductIndexZero ? (
              <td rowSpan={order.products.length}>{order.purchasedOn}</td>
            ) : null}
          </tr>
        );

        ordersArr.push(row);
      });
    });

    setOrders(ordersArr);
  }, [userOrders]);

  return (
    <>
      <h1 className="text-center my-4">Orders</h1>

      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Total Amount</th>
            <th>Purchase Date</th>
          </tr>
        </thead>

        <tbody>{orders}</tbody>
      </Table>
    </>
  );
}
