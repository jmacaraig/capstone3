import React from 'react';
import { useState, useEffect, useContext, useRef } from 'react';
import { Container, Table, Button } from 'react-bootstrap';
import UserContext from '../../../utils/UserContext';
import Swal from 'sweetalert2';
import { useNavigate, Navigate } from 'react-router-dom';

import CreateOrder from './CreateOrder';

export default function UserCart() {
	const { user } = useContext(UserContext);

	const userId = user.id;

	const [cart, setCart] = useState([]);
	const [totalCost, setTotalCost] = useState(0);
	const [orderedItems, setOrderedItems] = useState([]);

	const checkboxRefs = useRef([]);

	const increaseCart = (prodId, item) => {
		fetch(
			`https://cpstn2-ecommerceapi-macaraig.onrender.com/products/${prodId}/increaseItemOnCart`,
			{
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				getUserCart();

				checkboxRefs.current.forEach((checkboxRef) => {
					if (checkboxRef.current && checkboxRef.current.checked) {
						checkboxRef.current.checked = false;

						setOrderedItems([]);
						setTotalCost(0);
					}
				});
			});
	};

	const decreaseCart = (prodId, item) => {
		fetch(
			`https://cpstn2-ecommerceapi-macaraig.onrender.com/products/${prodId}/decreaseItemOnCart`,
			{
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				getUserCart();

				checkboxRefs.current.forEach((checkboxRef) => {
					if (checkboxRef.current && checkboxRef.current.checked) {
						checkboxRef.current.checked = false;

						resetActive();
					}
				});
			});
	};

	const removeItem = (orderedItems) => {
		fetch(
			`https://cpstn2-ecommerceapi-macaraig.onrender.com/products/removeCartItem`,
			{
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
				body: JSON.stringify({
					activeItem: orderedItems,
				}),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (data.message === 'Item removed.') {
					Swal.fire({
						title: 'Success.',
						icon: 'success',
						text: 'Item successfully removed.',
					})
						.then(getUserCart())
						.then(resetActive());
				} else {
					Swal.fire({
						title: 'Something went wrong.',
						icon: 'error',
						text: data.message,
					});
				}
			});
	};

	const handleChange = (item, e) => {
		const isChecked = e.target.checked;

		if (isChecked) {
			setOrderedItems((cartItems) => [...cartItems, item]);
			setTotalCost((total) => total + parseInt(item.subTotal));
		} else {
			setOrderedItems((cartItems) =>
				cartItems.filter((i) => i.productId !== item.productId)
			);
			setTotalCost((total) => total - parseInt(item.subTotal));
		}
	};

	const resetActive = () => {
		setOrderedItems([]);
		setTotalCost(0);
	};

	const getUserCart = () => {
		fetch(
			`https://cpstn2-ecommerceapi-macaraig.onrender.com/users/${userId}/userCart`,
			{
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				const cartArr = data.map((cartItem, index) => {
					const checkboxIndex = index;
					checkboxRefs.current.push(React.createRef());

					return (
						<tr key={cartItem.name}>
							<td>{cartItem.name}</td>
							<td>{cartItem.description}</td>
							<td className='text-center'>
								<button
									className='mx-3 bg-secondary text-white px-2 rounded text-lg'
									onClick={() => {
										decreaseCart(cartItem.productId, cartItem);
									}}
								>
									-
								</button>
								{cartItem.quantity}
								<button
									className='mx-3 bg-secondary text-white px-2 rounded text-lg'
									onClick={() => {
										increaseCart(cartItem.productId, cartItem);
									}}
								>
									+
								</button>
							</td>
							<td>{cartItem.subTotal}</td>
							<td className='text-center'>
								<input
									type='checkbox'
									name={cartItem.name}
									onChange={(e) => {
										handleChange(cartItem, e);
									}}
									ref={checkboxRefs.current[checkboxIndex]}
								/>
								<Button
									className='mx-2 bg-danger'
									variant='danger'
									size='sm'
									onClick={() => {
										removeItem(orderedItems);
									}}
								>
									Remove
								</Button>
							</td>
						</tr>
					);
				});

				setCart(cartArr);
			});
	};

	useEffect(() => {
		getUserCart();
		console.log(orderedItems);
	}, [orderedItems]);

	return cart.length !== 0 ? (
		<>
			<Container>
				<Table bordered responsive hover striped>
					<thead>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th className='text-center'>Quantity</th>
							<th>Sub Total</th>
							<th className='text-center'>Select / Remove Items</th>
						</tr>
					</thead>
					<tbody>{cart}</tbody>
					<tfoot>
						<tr>
							<th colSpan='3'>Total</th>
							<td>{totalCost}</td>
							<td className='text-center'>
								<CreateOrder
									orders={orderedItems}
									total={totalCost}
									getCart={getUserCart}
									resetCost={resetActive}
								/>
							</td>
						</tr>
					</tfoot>
				</Table>
				<p className='text-center'>
					<em>
						<strong>Note:</strong> To make an order, please finalize the
						quantity before checking the checkbox. Do not adjust quantity once
						checkbox is checked. Thank you!
					</em>
				</p>
			</Container>
		</>
	) : (
		<Container>
			<h1>Cart is Empty</h1>
		</Container>
	);
}
