import { Container, Row, Col } from "react-bootstrap"
import { Link } from "react-router-dom"

export const Banner = () => {
  return (
    <Container>
        <Row>
          <Col className="flex flex-col lg:flex-row items-center">
            <div className="text my-5 col-lg-6 col-sm-12 p-4">
                <h1 className="text-5xl font-bold">The Ultimate Innovative Appliance and Equipment Store</h1>
                <p className="text-2xl my-7 p-2 text-justify">HAHA Innovations is the world's most popular and authoritative source for new and innovative designs of appliance, equipment and merchandise for fun. Find ratings and access to the newest products today. Go find some HAHA moments!</p>
                <Link to="/products" type="button" className="bg-blue-700 text-white hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-base px-5 py-2.5 mr-2 mb-2 no-underline focus:outline-none">Explore Products</Link>
            </div>
            <div className="visual my-5 lg:max-w-xl col-6 col-sm-12">
                <img className="rounded-lg max-h-full" src="./home-living-room-1.png" alt="Funny Living Room" />
            </div>
          </Col>
        </Row>
    </Container>
  )
}
