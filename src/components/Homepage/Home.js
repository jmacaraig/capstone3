import { useEffect } from "react";

import { Banner } from "./components/Banner";

export const HomePage = () => {

    const useTitle = (title) => {

        useEffect(() => {
            document.title = `${title} - HAHA`;
        }, [title]);
    
      return null;
    }

    useTitle("Shop & Laugh: SCAM Alert :D");

  return (
        <Banner />
  )
}