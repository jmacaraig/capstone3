import { useState, useEffect } from 'react';
import { Container, Table } from 'react-bootstrap';

import EditProduct from './EditProduct';
import ChangeStatus from './ChangeStatus';


export default function AdminProduct({ adProds, adminProds }) {


	const [products, setProducts] = useState([])

	useEffect(() => {
		const productsArr = adProds.map(adProd => {
			return (
				<tr key={adProd._id}>
					<td>{adProd._id}</td>
					<td>{adProd.name}</td>
					<td>{adProd.brand = "" ? `N/A` : `${adProd.brand}`}</td>
					<td>{adProd.type = "" ? `N/A` : `${adProd.type}`}</td>
					<td>{adProd.description}</td>
					<td>{adProd.price}</td>
					<td>{adProd.stocks}</td>
					<td className={adProd.isActive ? "text-success" : "text-danger"}>
						{adProd.isActive ? "Available" : "Unavailable"}
					</td>
					<td className='text-center'><EditProduct product={adProd._id} adminProds={adminProds}/><ChangeStatus product={adProd._id} prodStat={adProd.isActive} adminProds={adminProds} prodStock ={adProd.stocks}/></td>
				</tr>
				)
		})

		setProducts(productsArr)

	}, [adProds])


	return(
		<>
		<Container>
			<h1 className="text-center my-4"> Admin Dashboard</h1>
			
			<Table striped bordered hover responsive>
				<thead>
					<tr className="text-center">
						<th>ID</th>
						<th>Name</th>
						<th>Brand</th>
						<th>Type</th>
						<th>Description</th>
						<th>Price</th>
						<th>Stocks</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>

				<tbody>
					{products}
				</tbody>
			</Table>
			</Container>	
		</>

		)
}