import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../../../utils/UserContext';

export default function CreateProduct() {
	const navigate = useNavigate();

	const { user } = useContext(UserContext);

	const [name, setName] = useState('');
	const [brand, setBrand] = useState('');
	const [type, setType] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [stocks, setStocks] = useState(0);

	function addProduct(e) {
		e.preventDefault();

		const token = localStorage.getItem('token');

		fetch(
			'https://cpstn2-ecommerceapi-macaraig.onrender.com/products/createProduct',
			{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
				body: JSON.stringify({
					name: name,
					brand: brand,
					type: type,
					description: description,
					price: price,
					stocks: stocks,
					user: user.id,
				}),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (data.message !== 'Product Already Exists' || !data) {
					Swal.fire({
						icon: 'success',
						title: 'Product Added',
					});

					setName('');
					setDescription('');
					setPrice(0);
					setStocks(0);
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Please try again.',
						text: data.message,
					});
				}
			});
	}

	return user.isAdmin === true ? (
		<>
			<Container>
				<h1 className='my-5 text-center'>Create Product</h1>
				<Form onSubmit={(e) => addProduct(e)}>
					<Form.Group>
						<Form.Label>Name:</Form.Label>
						<Form.Control
							type='text'
							placeholder='Enter Name'
							required
							value={name}
							onChange={(e) => {
								setName(e.target.value);
							}}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Brand:</Form.Label>
						<Form.Control
							type='text'
							placeholder='Enter Brand (not required)'
							value={brand}
							onChange={(e) => {
								setBrand(e.target.value);
							}}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Type:</Form.Label>
						<Form.Control
							type='text'
							placeholder='Enter Type (not required)'
							value={type}
							onChange={(e) => {
								setType(e.target.value);
							}}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Description:</Form.Label>
						<Form.Control
							as='textarea'
							placeholder='Enter Description'
							required
							value={description}
							onChange={(e) => {
								setDescription(e.target.value);
							}}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Price:</Form.Label>
						<Form.Control
							type='number'
							placeholder='Enter Price'
							required
							value={price}
							onChange={(e) => {
								setPrice(e.target.value);
							}}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Stock(s):</Form.Label>
						<Form.Control
							type='number'
							placeholder='Enter Available Quantity'
							required
							value={stocks}
							onChange={(e) => {
								setStocks(e.target.value);
							}}
						/>
					</Form.Group>
					<Button type='submit' className='my-5 bg-primary'>
						Submit
					</Button>
				</Form>
			</Container>
		</>
	) : (
		<Navigate to='/products' />
	);
}
