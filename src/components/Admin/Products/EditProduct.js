import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditProduct({ product, adminProds }) {
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);

	const [showEdit, setShowEdit] = useState(false);

	const token = localStorage.getItem('token');

	const openEdit = (product) => {
		fetch(
			`https://cpstn2-ecommerceapi-macaraig.onrender.com/products/${product}/getSingleProduct`,
			{
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				console.log(data);

				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				setStocks(data.stocks);
			});

		setShowEdit(true);
	};

	const closeEdit = () => {
		setShowEdit(false);
		setName('');
		setDescription('');
		setPrice(0);
		setStocks(0);
	};

	const editProduct = (e, product) => {
		e.preventDefault();

		fetch(
			`https://cpstn2-ecommerceapi-macaraig.onrender.com/products/${product}/updateProduct`,
			{
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price,
					stocks: stocks,
				}),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				console.log(data);

				if (data.status === 'Success') {
					Swal.fire({
						title: 'Success!',
						icon: 'success',
						text: 'Product Successfully Updated',
					});
					closeEdit();
					adminProds();
				} else {
					Swal.fire({
						title: 'Error!',
						icon: 'error',
						text: 'Please try again',
					});
					closeEdit();
					adminProds();
				}
			});
	};

	return (
		<>
			<Button variant='primary' size='sm' onClick={() => openEdit(product)}>
				Edit
			</Button>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={(e) => editProduct(e, product)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId='prodName'>
							<Form.Label>Name</Form.Label>
							<Form.Control
								type='text'
								value={name}
								onChange={(e) => setName(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId='prodDescription'>
							<Form.Label>Description</Form.Label>
							<Form.Control
								type='text'
								value={description}
								onChange={(e) => setDescription(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId='prodPrice'>
							<Form.Label>Price</Form.Label>
							<Form.Control
								type='number'
								value={price}
								onChange={(e) => setPrice(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId='prodStocks'>
							<Form.Label>Stocks</Form.Label>
							<Form.Control
								type='number'
								value={stocks}
								onChange={(e) => setStocks(e.target.value)}
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant='secondary' onClick={closeEdit}>
							Close
						</Button>
						<Button variant='success' type='submit'>
							Submit
						</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
	);
}
