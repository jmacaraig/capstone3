import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ChangeStatus({
	product,
	prodStat,
	prodStock,
	adminProds,
}) {
	const archiveToggle = (productId) => {
		fetch(
			`https://cpstn2-ecommerceapi-macaraig.onrender.com/products/${productId}/archiveProduct`,
			{
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (data.status === 'Success') {
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Product successfully disabled',
					});
				} else {
					Swal.fire({
						title: 'Something Went Wrong',
						icon: 'Error',
						text: 'Please Try again',
					});
				}
			});
		adminProds();
	};

	const activateToggle = (productId) => {
		if (prodStock !== 0) {
			fetch(
				`https://cpstn2-ecommerceapi-macaraig.onrender.com/products/${productId}/activateProduct`,
				{
					method: 'PUT',
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`,
					},
				}
			)
				.then((res) => res.json())
				.then((data) => {
					if (data.status === 'Success') {
						Swal.fire({
							title: 'Success',
							icon: 'success',
							text: 'Product successfully enabled',
						});
					} else {
						Swal.fire({
							title: 'Something Went Wrong',
							icon: 'error',
							text: 'Please Try again',
						});
					}

					adminProds();
				});
		} else {
			Swal.fire({
				title: 'Something Went Wrong',
				icon: 'error',
				text: 'Please add stock(s) to activate',
			});
		}
	};

	return (
		<>
			{prodStat ? (
				<Button
					className='mx-2'
					variant='danger'
					size='sm'
					onClick={() => archiveToggle(product)}
				>
					Archive
				</Button>
			) : (
				<Button
					className='mx-2'
					variant='success'
					size='sm'
					onClick={() => activateToggle(product)}
				>
					Activate
				</Button>
			)}
		</>
	);
}
