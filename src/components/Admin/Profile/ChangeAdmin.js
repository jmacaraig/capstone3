import { Button, Modal, Form, Table } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';

export default function CreateOrder({
	getAll,
	changeId,
	isAdmin,
	firstName,
	lastName,
}) {
	const [showEdit, setShowEdit] = useState(false);

	const openEdit = () => {
		setShowEdit(true);
	};

	const closeEdit = () => {
		setShowEdit(false);
	};

	const changeAdmin = (e) => {
		e.preventDefault();

		fetch(
			`https://cpstn2-ecommerceapi-macaraig.onrender.com/users/updateAdmin`,
			{
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
				body: JSON.stringify({ changeId }),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (data.message === 'User has been updated as an admin.') {
					Swal.fire({
						title: 'User is now an Admin.',
						icon: 'success',
						text: 'User admin status changed.',
					})
						.then(getAll())
						.then(closeEdit());
				} else {
					Swal.fire({
						title: 'Something went wrong.',
						icon: 'error',
						text: data.message,
					});
				}
			});
	};

	const removeAdmin = (e) => {
		e.preventDefault();

		fetch(
			`https://cpstn2-ecommerceapi-macaraig.onrender.com/users/removeAdmin`,
			{
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
				body: JSON.stringify({ changeId }),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (data.message === 'User has been removed as an admin.') {
					Swal.fire({
						title: "User's Admin rights have been revoked.",
						icon: 'success',
						text: 'User admin status changed.',
					})
						.then(getAll())
						.then(closeEdit());
				} else {
					Swal.fire({
						title: 'Something went wrong.',
						icon: 'error',
						text: data.message,
					});
				}
			});
	};

	return (
		<>
			<Button
				className={`m-5 bg-${isAdmin ? 'primary' : 'danger'}`}
				size='sm'
				onClick={() => openEdit()}
			>
				{isAdmin ? 'Set As User' : 'Set As Admin'}
			</Button>
			<Modal show={showEdit} onHide={closeEdit}>
				<Form
					onSubmit={(e) => {
						isAdmin ? removeAdmin(e) : changeAdmin(e);
					}}
				>
					<Modal.Header closeButton>
						<Modal.Title className='text-center'>
							{isAdmin
								? `Are you sure you want to remove user ${firstName} ${lastName} as Admin?`
								: `Are you sure you want to set user ${firstName} ${lastName} as Admin?`}
						</Modal.Title>
					</Modal.Header>
					<Modal.Footer>
						<Button variant='secondary' onClick={closeEdit}>
							Close
						</Button>
						<Button variant='success' type='submit'>
							Submit
						</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
	);
}
