import {useState,useEffect, useContext} from 'react';
import {Row, Col, Container, Table} from 'react-bootstrap';
import UserContext from '../../../utils/UserContext';

import ChangeAdmin from './ChangeAdmin'


export default function UserProfile({profile, allUsers, getAll}){

    const {user} = useContext(UserContext)

    const [usersList, setUsersList] = useState([])

    useEffect(() => {

        const calculateInactiveDays = (lastLogin) => {
            const currentDate = new Date();
            const lastLoginDate = new Date(lastLogin);
            const timeDifference = currentDate - lastLoginDate;
            const daysInactive = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
            return daysInactive;
          };

        const usersArr = allUsers.map((allUser) => (
            <tr key={allUser._id}>
              <td>{allUser._id}</td>
              <td className='text-center'>
			  {allUser.firstName} {allUser.lastName}
			  <div className={`text-${allUser.isAdmin ? 'danger' : 'primary'} p-2 small`}>{allUser.isAdmin ? 'ADMIN' : 'NOT ADMIN'}</div>
			  </td>
              <td>{allUser.email}</td>
              <td className='text-center'>
				<ChangeAdmin getAll={getAll} changeId ={allUser._id} isAdmin={allUser.isAdmin} firstName={allUser.firstName} lastName={allUser.lastName}/>
             </td>
              <td>{allUser.lastLogin}</td>
              <td>{calculateInactiveDays(allUser.lastLogin)} days</td>
            </tr>
        ));
      
        setUsersList(usersArr);
      
      }, [allUsers]);


	return (
		<>
		<Container className='mt-3'>
			<Row>
				<Col className="p-3 bg-primary rounded-lg text-white">
					<h1 className="my-3 ">Profile</h1>
					<h2 className="mt-3">{`${profile.firstName} ${profile.lastName}`}</h2>
					<hr />
					<h4>Contacts</h4>
					<ul>
						<li>Email: {profile.email}</li>
                        <li>Contact Number: </li>
					</ul>
				</Col>
			</Row>
			<Row className='pt-2 my-4'>
				<Col>
					<h1>Reset Password</h1>
				</Col>
                <Col>
                    <h1>Update Profile</h1>
                </Col>
			</Row>

	</Container>
			
			<h1 className="text-center my-4">All Users List</h1>

			<Table striped bordered hover responsive>
				<thead>
					<tr className="text-center">
						<th>ID</th>
                        <th>Full Name</th>
						<th>Email</th>
                        <th>Action(s)</th>
						<th>Last Login</th>
						<th>Day(s) Inactive</th>
					</tr>
				</thead>

				<tbody>
					{usersList}
				</tbody>
			</Table>	

		</>


	)

}