import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';

export default function AdminOrder({ adminOrders }) {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    const ordersArr = [];
    
    adminOrders.forEach((adminOrder) => {
      const productRows = adminOrder.products.map((product, index) => (
        <tr key={`${adminOrder._id}-${index}`}>
          {index === 0 ? (
            <td rowSpan={adminOrder.products.length}>{adminOrder._id}</td>
          ) : null}
          <td>
            <div>
              <p>Name: {product.name}</p>
              <p>Description: {product.description}</p>
              <p>Price: {product.price}</p>
              <p>Quantity: {product.quantityOrdered}</p>
            </div>
          </td>
          {index === 0 ? (
            <>
              <td rowSpan={adminOrder.products.length}>{adminOrder.totalAmount}</td>
              <td rowSpan={adminOrder.products.length}>{adminOrder.purchasedOn}</td>
            </>
          ) : null}
        </tr>
      ));

      ordersArr.push(...productRows);
    });

    setOrders(ordersArr);
  }, [adminOrders]);

  return (
    <>
      <h1 className="text-center my-4">Dashboard</h1>

      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>Products</th>
            <th>Total Amount</th>
            <th>Purchase Date</th>
          </tr>
        </thead>

        <tbody>{orders}</tbody>
      </Table>
    </>
  );
}