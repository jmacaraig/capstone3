import { useEffect, useState, useContext } from 'react';
import UserContext from '../../utils/UserContext';
import UserOrder from '../Users/Orders/UserOrder';
import AdminOrder from '../Admin/Orders/AdminOrder';

export default function Order() {
	const { user } = useContext(UserContext);
	const token = localStorage.getItem('token');

	const [adminOrders, setAdminOrders] = useState([]);
	const [userOrders, setUserOrders] = useState([]);

	const getAdminOrders = () => {
		fetch(
			'https://cpstn2-ecommerceapi-macaraig.onrender.com/orders/allOrders',
			{
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				setAdminOrders(data);
			});
	};

	const getUserOrders = () => {
		fetch(
			'https://cpstn2-ecommerceapi-macaraig.onrender.com/orders/ordersList',
			{
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				setUserOrders(data);
			});
	};

	useEffect(() => {
		if (user.isAdmin) {
			getAdminOrders();
		} else {
			getUserOrders();
		}
	}, []);

	return (
		<>
			{user.isAdmin &&
			adminOrders.length !== 0 &&
			adminOrders.length !== undefined ? (
				<AdminOrder adminOrders={adminOrders} getAdminOrders={getAdminOrders} />
			) : userOrders.length !== 0 && userOrders.length !== undefined ? (
				<UserOrder userOrders={userOrders} getUserOrders={getUserOrders} />
			) : (
				<h1>NO ORDER(S) MADE YET</h1>
			)}
		</>
	);
}
