import { useState, useEffect, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { useTranslation } from 'react-i18next';

import LanguageSelector from '../../languages/LanguageSelector';
import UserContext from '../../utils/UserContext';

export default function AppNavbar() {

    const { t } = useTranslation();
    const { user } = useContext(UserContext);


return (
    
    <Navbar className='bg-gray-900 text-white' expand="lg">
        <Container fluid>
            <Navbar.Brand as={Link} to="/" className="d-none d-sm-block">
                <div className="d-flex align-items-center">
                    <img src='./HAHA2.png' style={{ height: 40 }} />
                    <img src='./Innov.png' style={{ height: 30 }} />
                </div>
            </Navbar.Brand>

            <Navbar.Toggle aria-controls="basic-navbar-nav" style={{ backgroundColor: 'white' }}/>
            <Navbar.Collapse id='basic-navbar-nav'>
                <Nav className="ms-auto">
                   {(user.id !== null) ?
                        user.isAdmin?
                        <>
                         <Navbar.Brand className="bg-danger p-2 text-white rounded">ADMIN</Navbar.Brand>
                            <Nav.Link className="text-white" as={Link} to="/home">{t('navbar.home')}</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to="/profile">Profile</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to="/products">Products</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to="/orders">Orders</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to="/createProduct">Add Product</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to="/logout">Logout</Nav.Link>
                            <LanguageSelector />
                        </>
                        :
                        <>
                            <Nav.Link className="text-white" as={Link} to="/home">{t('navbar.home')}</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to="/profile">Profile</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to="/products">Products</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to={`/users/${user.id}/cart`}>Cart</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to="/orders">Orders</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to="/logout">Logout</Nav.Link>
                            <LanguageSelector />
                        </>
                        : 
                        <>
                            <Nav.Link className="text-white" as={Link} to="/home">{t('navbar.home')}</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to="/products">Products</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to="/login">Login</Nav.Link>
                            <Nav.Link className="text-white" as={Link} to="/register">Register</Nav.Link>
                            <LanguageSelector />
                        </>
                   }
                    

                   </Nav>
            </Navbar.Collapse>
        </Container>
    </Navbar>
)
}

