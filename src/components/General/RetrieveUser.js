import { useEffect, useState, useContext } from 'react';
import UserContext from '../../utils/UserContext';
import UserProfile from '../Users/Profile/UserProfile';
import AdminProfile from '../Admin/Profile/AdminProfile';

export default function Profile() {
	const { user } = useContext(UserContext);
	const token = localStorage.getItem('token');

	const [profile, setProfile] = useState([]);
	const [allUsers, setAllUsers] = useState([]);

	const getUserDetails = () => {
		fetch(
			'https://cpstn2-ecommerceapi-macaraig.onrender.com/users/userDetails',
			{
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				setProfile(data);
			});
	};

	const getAllUsers = () => {
		fetch('https://cpstn2-ecommerceapi-macaraig.onrender.com/users/allUsers', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				setAllUsers(data);
			});
	};

	useEffect(() => {
		if (user.isAdmin) {
			getUserDetails();
			getAllUsers();
		} else {
			getUserDetails();
		}
	}, []);

	return (
		<>
			{user.isAdmin ? (
				<AdminProfile
					profile={profile}
					allUsers={allUsers}
					getAll={getAllUsers}
				/>
			) : (
				<UserProfile profile={profile} />
			)}
		</>
	);
}
