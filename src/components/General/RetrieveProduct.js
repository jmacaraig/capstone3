import { useEffect, useState, useContext } from 'react';
import UserContext from '../../utils/UserContext';
import UserProduct from '../Users/Products/UserProducts';
import AdminProduct from '../Admin/Products/AdminProduct';

export default function Product() {
	const { user } = useContext(UserContext);
	const token = localStorage.getItem('token');

	const [adProds, setAdProds] = useState([]);
	const [usProds, setUsProds] = useState([]);

	const adminProds = () => {
		fetch(
			'https://cpstn2-ecommerceapi-macaraig.onrender.com/products/getAllProducts',
			{
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				setAdProds(data);
			});
	};

	const userProds = () => {
		fetch(
			'https://cpstn2-ecommerceapi-macaraig.onrender.com/products/getActiveProducts'
		)
			.then((res) => res.json())
			.then((data) => {
				setUsProds(data);
			});
	};

	useEffect(() => {
		if (user.isAdmin) {
			adminProds();
		} else {
			adminProds();
			userProds();
		}
	}, []);

	return (
		<>
			{user.isAdmin && adProds.length !== 0 && adProds.length !== undefined ? (
				<AdminProduct adProds={adProds} adminProds={adminProds} />
			) : usProds.length !== 0 && usProds.length !== undefined ? (
				<UserProduct usProds={usProds} userProds={userProds} />
			) : (
				<h1>NO PRODUCT(S) AVAILABLE YET</h1>
			)}
		</>
	);
}
