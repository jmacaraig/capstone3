import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';

import UserContext from '../../utils/UserContext';

export default function ProductView() {
	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const userId = user.id;
	const { prodId } = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [brand, setBrand] = useState(0);
	const [type, setType] = useState(0);
	const [images, setImages] = useState([]);
	const [isAddingToCart, setIsAddingToCart] = useState(false);

	useEffect(() => {
		fetch(
			`https://cpstn2-ecommerceapi-macaraig.onrender.com/products/${prodId}/getSingleProduct`
		)
			.then((res) => res.json())
			.then((data) => {
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				setStocks(data.stocks);
				setBrand(data.brand);
				setType(data.type);
				setImages(data.images);
			});
	}, [prodId]);

	const addToCart = (prodId) => {
		setIsAddingToCart(true);

		return fetch(
			`https://cpstn2-ecommerceapi-macaraig.onrender.com/products/${prodId}/addToCart`,
			{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (data.message === 'Cart Updated.') {
					Swal.fire({
						title: 'Cart Updated.',
						icon: 'success',
						text: 'Item added to cart.',
					});
				} else {
					Swal.fire({
						title: 'Something went wrong.',
						icon: 'error',
						text: 'Please try again.',
					});
				}
			})
			.then(setIsAddingToCart(false));
	};

	return (
		<Container className='mt-5'>
			<Row>
				<Col lg={4}>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Brand:</Card.Subtitle>
						<Card.Text>{brand}</Card.Text>
						<Card.Subtitle>Type:</Card.Subtitle>
						<Card.Text>{type}</Card.Text>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Card.Subtitle>Stocks</Card.Subtitle>
						<Card.Text>{stocks}</Card.Text>
						{user.id !== null ? (
							<>
								<Link
									className='btn btn-danger btn-block mx-2'
									onClick={() => addToCart(prodId)}
									disabled={isAddingToCart}
								>
									Add To Cart
								</Link>
								<Link
									className='btn btn-primary btn-block mx-2'
									onClick={() =>
										addToCart(prodId).then(() =>
											navigate(`/users/${userId}/cart`)
										)
									}
									disabled={isAddingToCart}
								>
									Order Now
								</Link>
							</>
						) : (
							<Link className='btn btn-danger btn-block' to={`/login`}>
								Log in to Order
							</Link>
						)}
					</Card.Body>
				</Col>
			</Row>
		</Container>
	);
}
