import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

const LanguageSelector = () => {
  const { i18n } = useTranslation();
  const [showDropdown, setShowDropdown] = useState(false);

  const changeLanguage = (language) => {
    i18n.changeLanguage(language);
    setShowDropdown(false);
  };

  const toggleDropdown = () => {
    setShowDropdown(!showDropdown);
  };

  const flagImageStyle = {
    width: '32px',
    height: '32px', 
  };


  const flagButtonStyle = {
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    position: 'relative',
  };

 
  const dropdownStyle = {
    position: 'absolute',
    top: '100%',
    left: 0,
    display: showDropdown ? 'block' : 'none',
    backgroundColor: 'white',
    boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.1)',
    zIndex: 1,
  };

  return (
    <div className="language-selector">
      <button onClick={toggleDropdown} style={flagButtonStyle}>
        <img src={`./flag-${i18n.language}.png`} alt={i18n.language} style={flagImageStyle} />
        {showDropdown && (
          <div className="dropdown" style={dropdownStyle}>
            <span onClick={() => changeLanguage('en')}>
              <img src="./flag-en.png" alt="English" style={flagImageStyle} />
            </span>
            <span onClick={() => changeLanguage('ko')}>
              <img src="./flag-ko.png" alt="Korean" style={flagImageStyle} />
            </span>
            <span onClick={() => changeLanguage('zh')}>
              <img src="./flag-zh.png" alt="Chinese" style={flagImageStyle} />
            </span>
          </div>
        )}
      </button>
    </div>
  );
};

export default LanguageSelector;
