import { useLocation } from 'react-router-dom';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import { useState, useEffect } from 'react';

import AppNavbar from './components/General/AppNavbar.js';
import Register from './components/Users/Forms/Register.js';
import Login from './components/Users/Forms/Login.js';
import CartView from './components/Users/Orders/UserCart.js';
import Logout from './components/General/Logout.js';
import Products from './components/General/RetrieveProduct.js';
import CreateProduct from './components/Admin/Products/CreateProduct.js';
import ProductView from './components/General/ProductView.js';
import Orders from './components/General/RetrieveOrders.js';
import Profile from './components/General/RetrieveUser.js';
import { HomePage } from './components/Homepage/Home.js';
import { UserProvider } from './utils/UserContext';
import { Footer } from './components/General/Footer.js';

import './App.css';

function App() {
	const [user, setUser] = useState({
		id: null,
		isAdmin: null,
	});

	const token = localStorage.getItem('token');

	const unsetUser = () => {
		localStorage.clear();
	};

	useEffect(() => {
		if (token) {
			fetch(
				`https://cpstn2-ecommerceapi-macaraig.onrender.com/users/userDetails`,
				{
					headers: {
						Authorization: `Bearer ${token}`,
					},
				}
			)
				.then((res) => res.json())
				.then((data) => {
					if (data._id !== 'undefined') {
						console.log(`user is set`);

						setUser({
							id: data._id,
							isAdmin: data.isAdmin,
						});

						// Else set the user states to the initial values
					} else {
						console.log(`user is not set`);

						setUser({
							id: null,
							isAdmin: null,
						});
					}
				});
		}
	}, [token]);

	return (
		<UserProvider value={{ user, setUser, unsetUser }}>
			<BrowserRouter>
				<AppNavbar />
				<div className='app-content'>
					<Routes>
						<Route path='/home' element={<HomePage />} />
						<Route path='/' element={<Navigate to='/home' />} />
						<Route path='/register' element={<Register />} />
						<Route path='/login' element={<Login />} />
						<Route path='/logout' element={<Logout />} />
						<Route path='/profile' element={<Profile />} />
						<Route path='/createProduct' element={<CreateProduct />} />
						<Route exact path='/products/:prodId' element={<ProductView />} />
						<Route
							exact
							path='/users/:userId/cart'
							element={<CartView userId={user.id} />}
						/>
						<Route path='/products' element={<Products />} />
						<Route path='/orders' element={<Orders />} />
					</Routes>
				</div>
				<Footer />
			</BrowserRouter>
		</UserProvider>
	);
}

export default App;
